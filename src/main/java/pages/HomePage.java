package pages;


import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import utils.AppDataManager;

import static utils.AppDataManager.reports;


public class HomePage extends Page{


    public HomePage(PageManager pages, ExtentTest test) {
        super(pages, test);

    }



    @FindBy(id = "lst-ib")
    private WebElement searchForm;

    @FindBy(xpath = "//input[@class='lsb'][1]")
    private WebElement submitButton;

    @FindBy(xpath = "//*[@id=\"rso\"]/div[1]/div/div[1]//cite")
    private WebElement firstSearchResult;

    public HomePage clickOnSearchForm() {
        test = AppDataManager.reports.startTest("Verify first search result link");
        searchForm.click();
        test.log(LogStatus.INFO, "Click on search form");
        log.debug("Clicking on search form");


        return this;
    }

    public HomePage typeText(String text) {
        searchForm.sendKeys(text);
        log.debug("Typing onto search form");
        return this;
    }

    public void submitForm() {
        submitButton.click();
        log.debug("Submitting form");
    }

    public String getLinkText() {
        log.debug("First search result link text is " + firstSearchResult.getText());
        return firstSearchResult.getText();
    }




}
