package utils;

import com.relevantcodes.extentreports.ExtentReports;

public class ExtentReport {

    public static ExtentReports getInstance() {
        ExtentReports extent;
        String Path = "C://Reports//BasicStructure//report-demo.html";
        extent = new ExtentReports(Path, true);
        extent
                .addSystemInfo("Selenium Version", "2.52")
                .addSystemInfo("Platform", "Windows");

        return extent;
    }
}
