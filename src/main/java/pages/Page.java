package pages;


import com.relevantcodes.extentreports.ExtentTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public abstract class Page {

    protected WebDriver driver;
    protected PageManager pages;
    ExtentTest test;
    public static final Logger log = LogManager.getLogger(Page.class.getName());

    public Page(PageManager pages, ExtentTest test) {
        this.pages = pages;
        this.test = test;
        driver = pages.getWebDriver();
    }

    public WebDriver getWebDriver() {
        return driver;
    }

}

