package utils;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import pages.Page;
import pages.PageManager;

import java.net.MalformedURLException;
import java.net.URL;
import org.testng.annotations.*;

public class AppDataManager {

    private String baseUrl;
    private WebDriver driver;
    private PageManager pageManager;
    public static ExtentReports reports;
    public ExtentTest test;
    private String nodeUrl;

    public AppDataManager(String platform, String browser) throws MalformedURLException {

        baseUrl = PropertyLoader.loadProperty("site.url");


        runInstance(platform, browser, baseUrl);

    }

    public AppDataManager(String browser) throws MalformedURLException {
        baseUrl = PropertyLoader.loadProperty("site.url");
        runInstance(browser, baseUrl);

    }
    @BeforeSuite
    public WebDriver setBrowser(String browser) {
        if (browser.equalsIgnoreCase("chrome")) {
            driver = new ChromeDriver();
        }
        if (browser.equalsIgnoreCase("firefox")) {
            driver = new FirefoxDriver();
        }
        return driver;
    }

    public void runInstance(String browser, String baseUrl) {
       /* System.setProperty("webdriver.chrome.driver",
                "C:\\AutomationBrowserDrivers\\chromedriver.exe");*/

        driver = setBrowser(browser);
        driver.get(baseUrl);
        reports = ExtentReport.getInstance();
        pageManager = new PageManager(driver);
    }
    public void runInstance(String platform, String browser, String baseUrl) throws MalformedURLException {

// Platforms
        DesiredCapabilities caps = new DesiredCapabilities();
        if (platform.equalsIgnoreCase("Windows")) {
            caps.setPlatform(Platform.WINDOWS);
        }
        if (platform.equalsIgnoreCase("MAC")) {
            caps.setPlatform(Platform.MAC);
        }
        // Browsers
        if (browser.equalsIgnoreCase("chrome")) {
            caps = DesiredCapabilities.chrome();
        }
        if (browser.equalsIgnoreCase("firefox")) {
            caps = DesiredCapabilities.firefox();
        }


        /*-------------selenium grid node config-----------------*/
        nodeUrl = "http://192.168.0.100:5555/wd/hub";

        driver = new RemoteWebDriver(new URL(nodeUrl), caps);
        /*--------------------------------------------------------*/

        driver.get(baseUrl);
        reports = ExtentReport.getInstance();
        pageManager = new PageManager(driver);
    }

    public PageManager getPageManager() {
        return pageManager;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void stop() {
        if (driver != null) {
            driver.quit();
        }
    }

    public void stopReports() {
        reports.endTest(test);
        reports.flush();
    }

}
