
import org.testng.annotations.*;
import utils.AppDataManager;

import java.net.MalformedURLException;

public class BaseTest {



    protected AppDataManager appDataManager;

    @Parameters({"browser"})
    @BeforeClass
    public void initWebDriver(String browser) throws MalformedURLException {
        appDataManager = new AppDataManager(browser);
    }


    //*@Parameters ({"platform","browser" })
   // @BeforeClass
    public void initWebDriver(String platform, String browser) throws MalformedURLException {
        appDataManager = new AppDataManager(platform, browser);

    }




    @AfterTest
    public void stop() {
        appDataManager.stop();
        appDataManager.stopReports();
    }

}

