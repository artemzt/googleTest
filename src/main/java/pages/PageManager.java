package pages;

import com.relevantcodes.extentreports.ExtentTest;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class PageManager {

    protected WebDriver driver;
    public HomePage homePage;
    public ExtentTest test;

    public PageManager(WebDriver driver) {
        this.driver = driver;
        homePage = initializePageObjects(new HomePage(this, test));
    }


    private <T extends Page> T initializePageObjects(T page) {
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), page);
        return page;
    }

    public WebDriver getWebDriver() {
        return driver;
    }


}
