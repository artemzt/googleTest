
import com.relevantcodes.extentreports.LogStatus;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utils.ExtentReport;


public class GoogleSearch extends BaseTest {


    @Test
    public void GoogleSearchTest() {
        //appDataManager.test = appDataManager.reports.startTest("Verify first search result link");
        appDataManager.getPageManager().homePage
                         .clickOnSearchForm()
                         .typeText("Google")
                         .submitForm();

        Assert.assertEquals(appDataManager.getPageManager().homePage.getLinkText(), "https://www.google.com.ua/?hl=ru" , "Check link for the first element on the list");
        //appDataManager.test.log(LogStatus.PASS, "Link corresponds to expected: www.google.ru/");


    }
}
